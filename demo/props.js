function generateKeyValues(key, value, extra) {
  const result = {};
  for (let i = 0; i < extra; i++) {
    result[`${key}_${i}`] = `${value}_${i}`;
  }
  return result;
}

function generateNamespace(extra = 100) {
  return [
    {
      name: "App",
      locales: [
        {
          lang: "en",
          translations: {
            value: "Value",
            key: "Key",
            ...generateKeyValues("example-key", "Example", extra),
          },
        },
        {
          lang: "de",
          translations: {
            value: "Wert",
            key: "__STRING_NOT_TRANSLATED__",
            ...generateKeyValues("example-key", "Beispiel", extra),
          },
        },
        {
          lang: "ru",
          translations: {
            value: "",
            key: "Ключ",
            ...generateKeyValues("example-key", "Пример", extra),
          },
        },
      ],
    },
    {
      name: "Auth",
      locales: [
        {
          lang: "en",
          translations: {
            login: "Login",
          },
        },
        {
          lang: "it",
          translations: {},
        },
      ],
    },
  ];
}
