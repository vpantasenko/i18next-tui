import React from "react";
import { isEqual } from "../helper";
import { DownloadButton } from "./download-button";
import { ToolbarItem } from "./toolbar-item";

interface IProps {
  valueList: string[];
  onDownload: (value: string) => void;
}

export class DownloadButtonList extends React.Component<IProps> {
  public shouldComponentUpdate(nextProps: IProps) {
    if (isEqual(nextProps.valueList, this.props.valueList)) {
      return false;
    }
    return true;
  }

  public render() {
    const downloadButtonList = this.props.valueList.map(lng => (
      <DownloadButton
        key={lng}
        value={lng}
        onClick={this.props.onDownload}
      />
    ));

    return (
      <ToolbarItem label="Download JSON">
        <div className="locales">{downloadButtonList}</div>
      </ToolbarItem>
    );
  }
}
