import { mount, ReactWrapper } from "enzyme";
import React from "react";
import { DownloadButtonList } from "../../src/toolbar/download-button-list";

describe("DownloadButtonList", () => {
  let wrapper: ReactWrapper<
    typeof DownloadButtonList.prototype.props,
    {},
    DownloadButtonList
  >;
  let handleDownload: (lang: string) => void;

  beforeEach(() => {
    handleDownload = jest.fn();
    wrapper = mount(
      <DownloadButtonList
        valueList={["en", "de", "fr"]}
        onDownload={handleDownload}
      />,
    );
  });

  it("renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("shouldn't update on the same props", () => {
    const result = wrapper.instance().shouldComponentUpdate({
      onDownload: handleDownload,
      valueList: ["en", "de", "fr"],
    });
    expect(result).toBe(false);
  });

  it("should update on different props", () => {
    const result = wrapper.instance().shouldComponentUpdate({
      onDownload: handleDownload,
      valueList: ["de", "en"],
    });
    expect(result).toBe(true);
  });

  it("should download when click on a button with specific language", () => {
    const button = wrapper
      .find({
        value: "en",
      })
      .find("button");
    button.simulate("click");
    expect(handleDownload).toHaveBeenCalledTimes(1);
    expect(handleDownload).toHaveBeenCalledWith("en");
  });
});
