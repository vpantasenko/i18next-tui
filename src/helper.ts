export function isEqual(a: string[], b: string[]) {
  if (!a || !b) {
    return false;
  }

  if (a.length !== b.length) {
    return false;
  }

  for (let i = 0, l = a.length; i < l; i++) {
    if (a[i] !== b[i]) {
      return false;
    }
  }
  return true;
}

export function isEmpty(v: string, defaultEmptyPlaceholder?: string): boolean {
  if (!v) {
    return true;
  }
  if (v === "") {
    return true;
  }
  if (defaultEmptyPlaceholder && v === defaultEmptyPlaceholder) {
    return true;
  }
  return false;
}
