import { saveAs } from "browser-filesaver";

class ServiceSingleton {
  public preventWidnowReload() {
    window.onbeforeunload = (e: any) => {
      if (!e) {
        e = window.event;
      }

      // e.cancelBubble is supported by IE - this will kill the bubbling process.
      e.cancelBubble = true;
      e.returnValue = "You sure you want to leave/refresh this page?";

      // e.stopPropagation works in Firefox.
      if (e.stopPropagation) {
        e.stopPropagation();
        e.preventDefault();
      }
    };
  }

  public downloadJson(namespace: I18NextTUI.INamespace, language: string) {
    const translations = this.getTranslations(namespace, language);

    const jsonFile = new Blob([JSON.stringify(translations, null, 2)], {
      type: "application/json;charset=utf-8",
    });

    saveAs(jsonFile, language + ".json");
  }

  public getTranslationByKey(
    namespace: I18NextTUI.INamespace,
    language: string,
    key: string,
  ): string {
    const translations = this.getTranslations(namespace, language);
    return translations[key] || "";
  }

  public getTranslations(
    namespace: I18NextTUI.INamespace,
    language: string,
  ): I18NextTUI.ITranslations {
    const locale = this.getLocale(namespace, language);
    return locale.translations;
  }

  private getLocale(namespace: I18NextTUI.INamespace, language: string): I18NextTUI.ILocale {
    const locale = namespace.locales.find(v => v.lang === language);
    if (!locale) {
      throw new Error("Locale was not found");
    }
    return locale;
  }
}

export const service = new ServiceSingleton();
