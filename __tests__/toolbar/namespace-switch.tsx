import { mount, ReactWrapper } from "enzyme";
import React from "react";
import { NamespaceSwitch } from "../../src/toolbar/namespace-switch";

describe("NamespaceSwitch", () => {
  let wrapper: ReactWrapper<
    typeof NamespaceSwitch.prototype.props,
    {},
    NamespaceSwitch
  >;
  let handleChange: (lang: string) => void;

  beforeEach(() => {
    handleChange = jest.fn();
    wrapper = mount(
      <NamespaceSwitch
        current={"general"}
        list={["general", "auth"]}
        onChange={handleChange}
      />,
    );
  });

  it("renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("shouldn't update on the same props", () => {
    const result = wrapper.instance().shouldComponentUpdate({
      current: "general",
      list: ["general", "auth"],
      onChange: handleChange,
    });
    expect(result).toBe(false);
  });

  it("should update on different props", () => {
    const result = wrapper.instance().shouldComponentUpdate({
      current: "general",
      list: ["general", "main"],
      onChange: handleChange,
    });
    expect(result).toBe(true);
  });

  it("should update on different active item", () => {
    const result = wrapper.instance().shouldComponentUpdate({
      current: "auth",
      list: ["general", "auth"],
      onChange: handleChange,
    });
    expect(result).toBe(true);
  });

  it("should download when click on a button with specific language", () => {
    const select = wrapper.find("select");
    select.simulate("change", {
      target: {
        value: "auth",
      },
    });
    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith("auth");
  });
});
