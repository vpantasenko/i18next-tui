import React, { ChangeEvent } from "react";
import { isEqual } from "../helper";
import { ToolbarItem } from "./toolbar-item";

interface IProps {
  list: string[];
  current: string;
  onChange: (namespace: string) => void;
}

export class NamespaceSwitch extends React.Component<IProps> {
  public shouldComponentUpdate(nextProps: IProps) {
    if (
      isEqual(nextProps.list, this.props.list) &&
      nextProps.current === this.props.current
    ) {
      return false;
    }
    return true;
  }

  public render() {
    const optionList = this.props.list.map(name => (
      <option value={name} key={name}>
        {name}
      </option>
    ));

    return (
      <ToolbarItem label="Namespace">
        <select
          value={this.props.current}
          onChange={this.handleChange}
          className="namespace-selector"
        >
          {optionList}
        </select>
      </ToolbarItem>
    );
  }

  private handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    event.preventDefault();
    this.props.onChange(event.target.value);
  }
}
