import { mount, ReactWrapper } from "enzyme";
import React from "react";
import { EditorWrapper } from "../../src/toolbar/editor-wrapper";

describe("EditorWrapper", () => {
  let wrapper: ReactWrapper<
    typeof EditorWrapper.prototype.props,
    {},
    EditorWrapper
  >;
  let handleChange: (lang: string) => void;

  beforeEach(() => {
    handleChange = jest.fn();
    wrapper = mount(
      <EditorWrapper
        currentLanguage={"en"}
        languageList={["en", "de"]}
        onChangeLanguage={handleChange}
      >
        <span />
      </EditorWrapper>,
    );
  });

  it("renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should download when click on a button with specific language", () => {
    const select = wrapper.find("select");
    select.simulate("change", {
      target: {
        value: "de",
      },
    });
    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith("de");
  });
});
