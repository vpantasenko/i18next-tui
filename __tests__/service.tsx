import { service } from "../src/service";

describe("Service", () => {
  it("prevents window reload", () => {
    service.preventWidnowReload();
    window.event = {
      cancelBubble: false,
      preventDefault: jest.fn(),
      returnValue: "123",
      stopPropagation: jest.fn(),
    } as any;
    expect(window.onbeforeunload).toBeDefined();
    if (!window.onbeforeunload) {
      fail();
      return;
    }
    const event = window.event as any;
    window.onbeforeunload(null as any);
    expect(event.cancelBubble).toBe(true);
    expect(event.returnValue).toBe(
      "You sure you want to leave/refresh this page?",
    );
    expect(event.stopPropagation).toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
  });
});
