import { saveAs } from "browser-filesaver";
import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { Table } from "../src/table";
import { DownloadButtonList } from "../src/toolbar/download-button-list";
import { Editor } from "../src/toolbar/editor";
import { EditorWrapper } from "../src/toolbar/editor-wrapper";
import { NamespaceSwitch } from "../src/toolbar/namespace-switch";
import { Translator } from "../src/translator";

jest.mock("browser-filesaver", () => ({
  saveAs: jest.fn(),
}));

function mockProps() {
  return [
    {
      locales: [
        {
          lang: "en",
          translations: {
            key: "Key",
            value: "Value",
          },
        },
        {
          lang: "de",
          translations: {
            key: "__STRING_NOT_TRANSLATED__",
            value: "Wert",
          },
        },
        {
          lang: "ru",
          translations: {
            key: "Ключ",
            value: "",
          },
        },
      ],
      name: "App",
    },
    {
      locales: [
        {
          lang: "en",
          translations: {
            login: "Login",
          },
        },
        {
          lang: "ru",
          translations: {} as any,
        },
      ],
      name: "Auth",
    },
  ];
}

describe("Translator", () => {
  let wrapper: ShallowWrapper<
    typeof Translator.prototype.props,
    {},
    Translator
  >;

  beforeEach(() => {
    wrapper = shallow(
      <Translator
        namespaceList={mockProps()}
        defaultEmptyPlaceholder={"__STRING_NOT_TRANSLATED__"}
      />,
    );
  });

  it("renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("renders correctly for empty namespaces", () => {
    wrapper.setProps({
      namespaceList: [],
    });
    expect(wrapper).toMatchSnapshot();
  });

  it("set namespace from props", () => {
    const switcher = wrapper.find(NamespaceSwitch);
    const propNs = wrapper.instance().props.namespaceList[1];
    switcher.props().onChange(propNs as any);
    const selectedRowKey = wrapper.state("selectedRowKey");
    const namespace = wrapper.state<I18NextTUI.INamespace>("namespace");
    expect(selectedRowKey).toBe("login");
    expect(namespace.name).toBe("Auth");
  });

  it("switches namespaces", () => {
    const switcher = wrapper.find(NamespaceSwitch);
    switcher.props().onChange("Auth");
    const selectedRowKey = wrapper.state("selectedRowKey");
    const namespace = wrapper.state<I18NextTUI.INamespace>("namespace");
    expect(selectedRowKey).toBe("login");
    expect(namespace.name).toBe("Auth");
  });

  it("fails to switch invalid namespaces", () => {
    const switcher = wrapper.find(NamespaceSwitch);
    function check() {
      switcher.props().onChange("Unexpected");
    }
    expect(check).toThrow();
  });

  it("changes language", () => {
    const editorWrapper = wrapper.find(EditorWrapper);
    editorWrapper.props().onChangeLanguage("de");
    const editorLanguage = wrapper.state("editorLanguage");
    expect(editorLanguage).toBe("de");
  });

  it("changes selected row", () => {
    const table = wrapper.find(Table);
    let selectedRowKey = wrapper.state<string>("selectedRowKey");
    expect(selectedRowKey).toBe("key");
    table.props().onSelect("value");
    selectedRowKey = wrapper.state("selectedRowKey");
    expect(selectedRowKey).toBe("value");
  });

  it("doesn't change if current row", () => {
    const table = wrapper.find(Table);
    const selectedRowKey = wrapper.state<string>("selectedRowKey");
    table.props().onSelect(selectedRowKey);
    expect(selectedRowKey).toBe("key");
  });

  it("changes value for selected row", () => {
    const editorWrapper = wrapper.find(Editor);
    editorWrapper.props().onChange("key", "NewValue");
    const editorLanguage = wrapper.state<string>("editorLanguage");
    const namespace = wrapper.state<I18NextTUI.INamespace>("namespace");
    expect(namespace.changed).toBeDefined();
    expect(namespace.changed && namespace.changed.has("key")).toBeTruthy();
    const locale = namespace.locales.find(v => v.lang === editorLanguage);
    if (!locale) {
      fail();
      return;
    }
    expect((locale.translations as any)["key"]).toBe("NewValue");
  });

  it("toggle empty filter", () => {
    const toggler = wrapper.find("input");
    toggler.simulate("change", {
      target: {
        checked: true,
      },
    });
    const selectedRowKey = wrapper.state("selectedRowKey");
    const filterEmpty = wrapper.state<boolean>("filterEmpty");
    expect(selectedRowKey).toBe("");
    expect(filterEmpty).toBeTruthy();
  });

  it("download specific lang translations as JSON", () => {
    const buttons = wrapper.find(DownloadButtonList);
    buttons.props().onDownload("en");
    // const saveAs = require("browser-filesaver").saveAs;
    expect(saveAs).toHaveBeenCalledTimes(1);
    const jsonFile = new Blob(
      [
        JSON.stringify(
          {
            key: "Key",
            value: "Value",
          },
          null,
          2,
        ),
      ],
      {
        type: "application/json;charset=utf-8",
      },
    );
    expect(saveAs).toHaveBeenCalledWith(jsonFile, "en.json");
  });
});
